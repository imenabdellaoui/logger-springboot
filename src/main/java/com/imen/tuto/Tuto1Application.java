 package com.imen.tuto;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
public class Tuto1Application {
	   private static final Logger logger = LoggerFactory.getLogger(Tuto1Application .class);
		public static void main(String[] args) {
	   SpringApplication.run(Tuto1Application.class, args);
		logger.info("this is a info message");
	      logger.warn("this is a warn message");
	      logger.error("this is a error message");
	      logger.debug("this is a debug message");
}
		@RequestMapping("/")
		public String welcome(){
			String name="kk";
			if(name.length()==2){
				throw new RuntimeException("Opps exception has occured");
			}
			return "Hello World!!";
		}		
		
		
		
}



