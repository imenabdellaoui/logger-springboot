package com.imen.tuto.entites;

import javax.persistence.Entity;

import java.io.Serializable;

import javax.persistence.*;

@Entity
public class Person implements Serializable {
												 
	@Id
	@GeneratedValue
    @Column(name="id_person")
	private Long personId;
	private String name;
	private String familyName;
	private String adresse;
	private String phone;
	private String mail;
	
	public Person() {
		 
	}
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFamilyName() {
		return familyName;
	}
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	
	
	
	

}
