package com.imen.tuto.controllers;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.imen.tuto.entites.Person;
import com.imen.tuto.repositories.PersonRepository;


@RestController
@RequestMapping(value = "/v1")
public class PersonController {
	@Autowired
	private PersonRepository personRepository;

	// private static final String List = null;

	@PostMapping(value = "/create")
	public Person CreatPerson(@RequestBody Person Person) {
		return personRepository.save(Person);
	}
 
	/**
	 * this method aims to do the update of selected person's instance
	 * @param id
	 * @param person
	 * @return the status of the update operation 
	 */
	@PutMapping(value = "/Update/{id}")
	public Person updatePerson(@PathVariable Long id, @RequestBody Person person) {

		if (id != null) {
			Person p = personRepository.findOne(id);
			if (p != null) {
				person.setPersonId(id);
				return personRepository.save(person);
			}
		}
		return null;
	}

	@DeleteMapping(value = "/delete/{id}")
	public void deletePerson(@PathVariable(value = "id") Long id) {

		if (id != null) {
			Person person = personRepository.findOne(id);
			if (person != null) {
				personRepository.delete(id);
			}
		}
	}

	// @GetMapping("/get/{id}")
	// public List<Person> getAll()

	// {
	// return PersonRepo.findAll(id);

//}
     
	@GetMapping(value = "/all")
	public List<Person> getAll() {
		return personRepository.findAll();
	}
	@GetMapping(value="/all/by/name/{name}")
      public List<Person> getByName(@PathVariable String name){
		return personRepository.getByName(name);
	}
	
	@GetMapping(value="/all/{adresse}")
    public List<Person> getByAdresse(@PathVariable String adresse){
		
		return personRepository.getByAdresse(adresse);	
		 
	}
	
	
	
	
	
	
	
	
}
