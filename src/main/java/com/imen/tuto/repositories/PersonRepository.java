package com.imen.tuto.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.imen.tuto.entites.Person;

public interface PersonRepository extends  JpaRepository<Person, Long>  {
	public List<Person> getByName(String name);
	public List<Person> getByAdresse(String adresse);
	}
